import React, {Component} from 'react';
import AppRoutes from "./components/AppRoutes/AppRoutes";

import './App.css';
class App extends Component {

    render() {
        return (
            <div className="app">
                <AppRoutes/>
            </div>
        );
    }
}

export default App;
