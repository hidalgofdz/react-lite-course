import React, {Component} from 'react';
import PropTypes from 'prop-types';

import Grid from "../../../../../../components/Grid/Grid";
import * as githubClient from "../../../../../../services/githubClient/githubClient";
import ContributorCard from "./ContributorCard/ContributorCard";

class ContributorsGrid extends Component {

    state = {
        contributorsList: [],
    };

    componentDidMount() {
        const {owner, repository} = this.props;
        this.obtainContributorsList(owner, repository);
    }

    obtainContributorsList = (owner, repository) => {
        this.setState({contributorsListLoading: true});
        githubClient.getRepositoryContributors(owner, repository)
            .then(contributorsList => {
                this.setState({
                    contributorsList: [...contributorsList],
                    contributorsListLoading: false
                });
            })
    };

    render() {
        const {contributorsList} = this.state;
        return (
            <Grid>
                {contributorsList.map(contributor => (
                    <ContributorCard
                        key={contributor.loginName}
                        contributor={contributor}
                    />
                ))}
            </Grid>
        );
    }

}


ContributorsGrid.propTypes = {
    owner: PropTypes.string.isRequired,
    repository: PropTypes.string.isRequired,
};

export default ContributorsGrid;