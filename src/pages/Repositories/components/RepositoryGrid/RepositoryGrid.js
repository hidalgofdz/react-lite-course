import React from "react";
import PropTypes from 'prop-types';
import {NavLink} from 'react-router-dom';

import RepositoryCard from "./components/RepositoryCard/RepositoryCard";
import Grid from "../../../../components/Grid/Grid";

import './RepositoriesGrid.css';


const RepositoriesGrid = ({repositoriesList}) => {
    return (
        <Grid noItemsMessage="No results found">
            {repositoriesList.map(repository => (
                <NavLink
                    className="repositories__link"
                    to={`/${repository.repoUrl}`}
                    key={repository.id}
                >
                    <RepositoryCard
                        repository={repository}
                    />
                </NavLink>
            ))}
        </Grid>
    );
};

RepositoriesGrid.propTypes = {
    repositoriesList: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        repoUrl: PropTypes.string.isRequired,
    })).isRequired
};

export default RepositoriesGrid;
